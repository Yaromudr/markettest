package Release;

import ru.goblin.testing.market.pages.AbstractYandexPage;
import ru.goblin.testing.market.utilites.LightDriver;
import ru.goblin.testing.market.utilites.db.DBUtils;
import ru.goblin.testing.market.utilites.db.Statements;

import java.util.ArrayList;

public abstract class BasicTest {
    protected static LightDriver driver;
    protected AbstractYandexPage page;
    protected static ArrayList<String> vendorList = new ArrayList<>();
    protected static ArrayList<String> priceList = new ArrayList<>();
    protected static String type;

    protected static void init(String type) {
        driver = new LightDriver();
        driver.manage().window().maximize();
        BasicTest.type = type;

        DBUtils.getConnection();
        DBUtils.execQuery(Statements.DROP_TABLE_VENDOR);
        DBUtils.execQuery(Statements.CREATE_VENDOR_TABLE);
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "Beats", "Наушники");
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "Samsung", "Телевизоры");
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "LG", "Телевизоры");
        DBUtils.selectQueryByType(Statements.SELECT_VENDOR_BY_TYPE, type);
        DBUtils.printResultSet(vendorList);

        DBUtils.execQuery(Statements.DROP_TABLE_PRICE);
        DBUtils.execQuery(Statements.CREATE_PRICE_TABLE);
        DBUtils.insertQuery(Statements.INSERT_PRICE_ITEM, "20000", "Телевизоры");
        DBUtils.insertQuery(Statements.INSERT_PRICE_ITEM, "5000", "Наушники");
        DBUtils.selectQueryByType(Statements.SELECT_PRICE_BY_TYPE, type);
        DBUtils.printResultSet(priceList);


    }

    protected static void after() {
        DBUtils.closeAll();
        driver.close();
    }
}
