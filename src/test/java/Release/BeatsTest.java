package Release;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.goblin.testing.market.pages.MainPage;
import ru.goblin.testing.market.pages.MarketPage;
import ru.goblin.testing.market.pages.SearchPage;
import ru.goblin.testing.market.utilites.AssertManager;

public class BeatsTest extends BasicTest {

    @BeforeTest
    public static void initDriver() {
        init("Наушники");
    }

    private String yandexHost = "https://www.yandex.ru/";
    private String item;

    @Test
    public void BeatsTest() {
        driver.get(yandexHost);
        AssertManager.assertTitle(driver, "Яндекс");
        page = new MainPage(driver);
        page.clickByCaption("Маркет");
        page = new MarketPage(driver);
        page.clickForMenu("Электроника");
        page.clickForLeftMenu(type);
        page.clickByCaption("Расширенный поиск");
        page = new SearchPage(driver);
        priceList.forEach(item -> page.setPrice("от", item));
        vendorList.forEach(item -> page.setCheckBox(item));
        page.clickByCaption("Применить");
        AssertManager.assertRecord(driver, driver.getWait(), 10);
        item = page.getFirstItem();
        page.search(item);
        AssertManager.assertNames(driver, driver.getWait(), item);
    }

    @AfterTest
    public void afterTest() {
        after();
    }
}
