package Debug;

import Release.BasicTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.goblin.testing.market.pages.MainPage;
import ru.goblin.testing.market.pages.MarketPage;
import ru.goblin.testing.market.pages.SearchPage;
import ru.goblin.testing.market.utilites.AssertManager;


public class TvTest_Debug extends BasicTest {
    @BeforeTest
    public static void initDriver() {
        init("тест");
    }

    private String yandexHost = "https://www.yandex.ru/";

    @Test
    public void startYandex() {
        driver.get(yandexHost);

        AssertManager.assertTitle(driver, "Яндекс");
        page = new MainPage(driver);
    }

    @Test(dependsOnMethods = "startYandex")
    public void goToMarket() {
        page.clickByCaption("Маркет");
        page = new MarketPage(driver);

    }

    @Test(dependsOnMethods = "goToMarket")
    public void goToElectronics() {
        page.clickByCaption("Электроника");
    }

    @Test(dependsOnMethods = "goToElectronics")
    public void goToTV() {
        page.clickByCaption("Телевизоры");
    }

    @Test(dependsOnMethods = "goToTV")
    public void goToExtSearch() {
        page.clickByCaption("расширенный поиск");
        page = new SearchPage(driver);
    }

    @Test(dependsOnMethods = "goToExtSearch")
    public void setPrice() {
        page.setPrice("от", "20000");
    }

    @Test(dependsOnMethods = "setPrice")
    public void setFilters() {
        page.setCheckBox("LG");
        page.setCheckBox("Samsung");
    }

    @Test(dependsOnMethods = "setFilters")
    public void submit() {
        page.clickByCaption("Применить");
    }

    @Test(dependsOnMethods = "submit")
    public void assertRecords() {
        AssertManager.assertRecord(driver, driver.getWait(), 10);
    }

    private String item;

    @Test(dependsOnMethods = "assertRecords")
    public void rememberFirstItem() {
        item = page.getFirstItem();
    }

    @Test(dependsOnMethods = "rememberFirstItem")
    public void searchItem() {
        page.search(item);
    }

    @Test(dependsOnMethods = "searchItem")
    public void assertItem() {
        AssertManager.assertNames(driver, driver.getWait(), item);
    }

    @AfterTest
    public void afterTest() {
        after();
    }
}
