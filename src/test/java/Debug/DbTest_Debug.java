package Debug;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.goblin.testing.market.utilites.db.DBUtils;
import ru.goblin.testing.market.utilites.db.Statements;

import java.util.ArrayList;

@Test
public class DbTest_Debug {
    private static ArrayList<String> list = new ArrayList<>();

    public static void run() {


        DBUtils.getConnection();
        DBUtils.execQuery(Statements.DROP_TABLE_VENDOR);
        DBUtils.execQuery(Statements.CREATE_VENDOR_TABLE);
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "Beats", "Наушники");
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "Samsung", "Телевизоры");
        DBUtils.insertQuery(Statements.INSERT_VENDOR_ITEM, "LG", "Телевизоры");
        DBUtils.selectQueryByType(Statements.SELECT_VENDOR_BY_TYPE, "Телевизоры");
        DBUtils.printResultSet(list);

        System.out.println(list);
        System.out.println("********************");
        list.clear();
        DBUtils.execQuery(Statements.DROP_TABLE_PRICE);
        DBUtils.execQuery(Statements.CREATE_PRICE_TABLE);
        DBUtils.insertQuery(Statements.INSERT_PRICE_ITEM, "20000", "Телевизоры");
        DBUtils.insertQuery(Statements.INSERT_PRICE_ITEM, "5000", "Наушники");
        DBUtils.selectQueryByType(Statements.SELECT_PRICE_BY_TYPE, "Телевизоры");
        DBUtils.printResultSet(list);

        System.out.println(list);

    }

    @AfterTest
    public static void after() {
        DBUtils.closeAll();
    }
}
