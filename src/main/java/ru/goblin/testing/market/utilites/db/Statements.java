package ru.goblin.testing.market.utilites.db;

/**
 * Класс содержит шаблоны запросов к БД
 */
public enum Statements {
    DROP_TABLE_VENDOR("DROP TABLE IF EXISTS VENDOR;"),
    DROP_TABLE_PRICE("DROP TABLE IF EXISTS PRICE;"),
    CREATE_VENDOR_TABLE("CREATE TABLE IF NOT EXISTS VENDOR  (ID INT AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(255), TYPE_NAME VARCHAR(255));"),
    CREATE_PRICE_TABLE("CREATE TABLE IF NOT EXISTS PRICE  (ID INT AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(255), TYPE_NAME VARCHAR(255));"),
    SELECT_VENDOR_BY_TYPE("SELECT NAME FROM VENDOR WHERE TYPE_NAME = ?;"),
    SELECT_PRICE_BY_TYPE("SELECT NAME FROM PRICE WHERE TYPE_NAME = ?;"),
    INSERT_VENDOR_ITEM("INSERT INTO VENDOR (NAME, TYPE_NAME) VALUES (?, ?);"),
    INSERT_PRICE_ITEM("INSERT INTO PRICE (NAME, TYPE_NAME) VALUES (?, ?);"),;


    private String statement;

    Statements(String statement) {
        this.statement = statement;
    }

    @Override
    public String toString() {
        return statement;
    }
}
