package ru.goblin.testing.market.utilites.db;


import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

/**
 * Created by user on 24.02.2016.
 */
public class DBUtils {
    private static Logger LOGGER = Logger.getLogger(DBUtils.class);

    private static Connection connection = null;
    private static PreparedStatement statement = null;
    private static ResultSet resultSet = null;

    private static String login = "sa";
    private static String pass = "";

    /**
     * Создание соединения к БД
     *
     * @param type создаваемый тип подключения: file/mem/zip/ssl и тд по документации на H2
     */
    public static void createConnection(String type) {
        try {
            connection = DriverManager.getConnection(getUrl("h2", type, "~/h2testing/h2db_mySql", "MySQL"), login, pass);
        } catch (SQLException e1) {
            LOGGER.error("Ошибка подключения: ", e1);
        }
    }

    /**
     * Если соединения нет, создает базу в памяти
     * @return соединение
     */
    public static Connection getConnection() {
        if (connection != null) return connection;
        else {
            createConnection("mem");
            return connection;
        }
    }

    /**
     * Собиралка адреса для подключения
     * @param vendor Тип БД, в нашем случае h2
     * @param type Тип подключения
     * @param path file/mem/zip/ssl
     * @param mode Для варианта мимикрии указываем диалект запросов
     * @return собранная строка для подключения
     */

    public static String getUrl(String vendor, String type, String path, String mode) {
        StringBuffer comboUrl = new StringBuffer();
        comboUrl.append("jdbc:")
                .append(vendor)
                .append(":");
        if (type.equals("mem")) {
            comboUrl.append(type).append(":");
        }
        comboUrl.append(path);
        if (mode != null && !mode.isEmpty()) {
            comboUrl.append(";MODE=")
                    .append(mode);
        }

        return comboUrl.toString();
    }

    /**
     * Метод выполняет запрос без параметров. Например запросы на создание таблицы и ее уничтожение.
     *
     * @param st - запрос
     */
    public static void execQuery(Statements st) {
        try {
            statement = connection.prepareStatement(st.toString());
            statement.execute();
            statement.executeUpdate();
        } catch (SQLException e1) {
            LOGGER.error("Ошибка выполнения запроса: ", e1);
        } finally {
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                LOGGER.error("Ошибка закрытия потоков: ", e);
            }
        }
    }

    /**
     * Метод выполняет Select запрос с одним параметром
     * @param st - запрос
     * @param param - параметр запроса
     * @return ResultSet
     */
    public static ResultSet selectQueryByType(Statements st, String param) {
        try {
            statement = connection.prepareStatement(st.toString());
            statement.setString(1, param);
            resultSet = statement.executeQuery();
            return resultSet;
        } catch (SQLException e1) {
            LOGGER.error("Ошибка выполнения запроса: ", e1);
        }
        return resultSet;
    }

    /**
     * Метод выполняет вставку значений в таблицу из 2 полей
     * @param st шаблон запроса
     * @param name первое поле
     * @param type второе поле
     */
    public static void insertQuery(Statements st, String name, String type) {
        try {
            statement = connection.prepareStatement(st.toString());

            statement.setString(1, name);
            statement.setString(2, type);
            statement.execute();

        } catch (SQLException e1) {
            LOGGER.error("Ошибка выполнения запроса: ", e1);
        } finally {
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                LOGGER.error("Ошибка закрытия потоков: ", e);
            }
        }
    }

    /**
     * Собираем лист с необходимыми результатами из запроса
     * @param list - собираемый список
     */
    public static void printResultSet(List<String> list) {
        try {
            while (resultSet.next()) {
                String item = resultSet.getString("name");
                System.out.println("Todo Item: " + item);
                list.add(item);
            }

        } catch (SQLException e1) {
            LOGGER.error("Ошибка вывода результата: ", e1);
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
            } catch (SQLException e) {
                LOGGER.error("Ошибка закрытия потоков: ", e);
            }
        }
    }

    /**
     * Закрытие всех соединений, если они еще открыты
     */
    public static void closeAll() {
        try {
            if (resultSet != null) resultSet.close();
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        } catch (SQLException e) {
            LOGGER.error("Ошибка закрытия потоков: ", e);
        }
    }
}
