package ru.goblin.testing.market.utilites;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LightDriver extends FirefoxDriver {

    private WebDriverWait wait;

    public LightDriver() {
        wait = new WebDriverWait(this, 10);
    }

    public WebDriverWait getWait() {
        return wait;
    }
}
