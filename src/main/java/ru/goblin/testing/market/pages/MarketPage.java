package ru.goblin.testing.market.pages;

import ru.goblin.testing.market.utilites.LightDriver;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static java.lang.String.format;

public class MarketPage extends AbstractYandexPage {
    private MarketPage self;


    public MarketPage(LightDriver driver) {
        super(driver);
        self = this;
    }

    @Override
    protected AbstractYandexPage getThis() {
        return self;
    }



    @Override
    public AbstractYandexPage clickByCaption(CharSequence text) {
        //расширенный поиск →
        return super.clickByCaption((format("//a[(contains(text(), '%s')) and ( contains(@class, 'link') or contains(@class, 'black'))]", text)));
    }

    @Override
    public String getFirstItem() {
        throw new NotImplementedException();
    }

    @Override
    public AbstractYandexPage clickForMenu(String text) {
        String xpath = format("//a[(contains(text(), '%s')) and (contains(@class,'topmenu__link')) and (contains(@class, 'link'))]", text);
        return super.clickForMenu(xpath);
    }

    @Override
    public AbstractYandexPage clickForLeftMenu(String text) {
        String xpath = format("//a[(contains(text(), '%s')) and (contains(@class,'catalog-menu__list-item')) and (contains(@class, 'link'))]", text);
        return super.clickForMenu(xpath);
    }
}
