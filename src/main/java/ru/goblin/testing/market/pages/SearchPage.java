package ru.goblin.testing.market.pages;

import ru.goblin.testing.market.utilites.LightDriver;

import static java.lang.String.format;

public class SearchPage extends AbstractYandexPage {
    private SearchPage self;


    @Override
    protected AbstractYandexPage getThis() {
       return self;
    }

    public SearchPage(LightDriver driver) {
        super(driver);
        self = this;
    }

    @Override
    public AbstractYandexPage clickByCaption(CharSequence text) {
        return super.clickByCaption(format("//button[child::span[contains(text(), '%s')]]", text));
    }

    @Override
    public AbstractYandexPage setCheckBox(CharSequence text) {
        return super.setCheckBox(format("//input[(parent::span[following-sibling::label[(contains(@class, 'checkbox__label')) and (contains(text(), '%s'))]]) and (ancestor::*[contains(@class, 'vendors-list__top')])]", text));
    }

    @Override
    public AbstractYandexPage setPrice(String label, String text) {
        return super.setPrice(format("//input[ancestor::span[contains(@sign-title, '%s')]]", label), text);
    }


    public String getFirstItem(){
        String xpath = "//span[contains(@class, 'snippet-card__header-text')]";
        return super.getFirstItem(xpath);
    }

}
