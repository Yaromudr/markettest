package ru.goblin.testing.market.pages;

import ru.goblin.testing.market.utilites.LightDriver;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static java.lang.String.format;


public class MainPage extends AbstractYandexPage {

    private MainPage self = null;


    public MainPage(LightDriver driver) {
        super(driver);
        self = this;
    }

    @Override
    protected AbstractYandexPage getThis() {
        return self;
    }

    @Override
    public AbstractYandexPage clickByCaption(CharSequence text) {
        return super.clickByCaption(format("//a[(contains(text(), '%s')) and ((contains(@class, 'b-link')) or (contains(@class, 'link')))]", text));
    }

    @Override
    public String getFirstItem() {
        throw new NotImplementedException();
    }
}
